const BASE_URL = "https://635f4b17ca0fe3c21a991d24.mockapi.io";
function fetchAllTodo() {
  axios({
    url: `${BASE_URL}/todos`,
    method: "GET",
  })
    .then(function (res) {
      console.log("res todos: ", res.data);
      renderTodoList(res.data);
      turnOffLoading();
    })
    .catch(function (err) {
      console.log(err);
      turnOnLoading();
    });
}
var idEdited = null;
fetchAllTodo();
function removeTodo(idTodo) {
  axios({
    url: `${BASE_URL}/todos/${idTodo}`,
    method: "DELETE",
  })
    .then(function (res) {
      turnOnLoading();
      fetchAllTodo();
    })
    .catch(function (err) {
      turnOnLoading();
    });
}
function addTodo() {
  var data = layThongTinTuForm();
  var newTodo = {
    name: data.name,
    desc: data.desc,
    isComplete: true,
  };
  axios({
    url: `${BASE_URL}/todos`,
    method: "POST",
    data: newTodo,
  })
    .then(function (res) {
      console.log("res todos: ", res.data);
      fetchAllTodo();
      turnOnLoading();
      resetInput();
    })
    .catch(function (err) {
      console.log(err);
      turnOnLoading();
    });
}
function editTodo(idTodo) {
  turnOnLoading();
  axios({
    url: `${BASE_URL}/todos/${idTodo}`,
    method: "GET",
  })
    .then(function (res) {
      console.log("res: ", res);
      //showThongTinLenForm
      document.getElementById("name").value = res.data.name;
      document.getElementById("desc").value = res.data.desc;
      turnOffLoading();
      idEdited = res.data.id;
    })
    .catch(function (err) {
      turnOnLoading();
      console.log("err: ", err);
    });
}
function updateTodo() {
  turnOnLoading();
  let data = layThongTinTuForm();
  axios({
    url: `${BASE_URL}/todos/${idEdited}`,
    method: "PUT",
    data: data,
  })
    .then(function (res) {
      fetchAllTodo();
      turnOffLoading();
      resetInput();
    })
    .catch(function (err) {
      turnOnLoading();
    });
}
