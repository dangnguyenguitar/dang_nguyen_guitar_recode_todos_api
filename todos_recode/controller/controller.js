function renderTodoList(todos) {
  var contentHTML = "";
  todos.forEach((item) => {
    var contentTr = `
        <tr>
        <td>${item.id}</td>
        <td>${item.name}</td>
        <td>${item.desc}</td>
        <td>
        <input type="checkbox" ${item.isComplete ? "checked" : ""} />
        </td>
        <td>
        <button onclick="removeTodo(${
          item.id
        })" class="btn btn-danger">Xoá</button>
        </td>
        <td>
        <button onclick="editTodo(${item.id})" class="btn btn-dark">Sửa</button>
        </td>
        </tr>
           `;
    contentHTML += contentTr;
  });
  document.getElementById("tbody-todos").innerHTML = contentHTML;
}
function turnOnLoading() {
  document.getElementById("loading").style.display = "flex";
}
function turnOffLoading() {
  document.getElementById("loading").style.display = "none";
}
function resetInput() {
  document.getElementById("name").value = "";
  document.getElementById("desc").value = "";
}
function layThongTinTuForm() {
  var name = document.getElementById("name").value;
  var desc = document.getElementById("desc").value;
  return {
    name: name,
    desc: desc,
  };
}
